import { Ng2EmotionGridPage } from './app.po';

describe('ng2-emotion-grid App', () => {
  let page: Ng2EmotionGridPage;

  beforeEach(() => {
    page = new Ng2EmotionGridPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
