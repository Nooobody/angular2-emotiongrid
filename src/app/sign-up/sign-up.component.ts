import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';

import { UserService } from '../user.service';
import { API_URL } from '../globals';
const url = API_URL + "user/sign_up"

@Component({
  selector: 'sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.sass']
})
export class SignUpComponent implements OnInit {

  private email = "";
  private username = "";
  private password = "";
  private password_again = "";

  constructor(private user : UserService) { }

  ngOnInit() {
  }

  private check_passwords() {
    return this.password === this.password_again;
  }

  private sign_up() {
    let data = {
      username: this.username,
      email: this.email,
      password: this.password
    }

    this.user.sign_up(data);
  }
}
