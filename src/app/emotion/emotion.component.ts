import { Component, OnInit, Input } from '@angular/core';
import { Emotion } from '../emotion'
import { EmotionTrackingComponent } from '../emotion-tracking/emotion-tracking.component';

@Component({
  selector: 'emotion',
  templateUrl: './emotion.component.html',
  styleUrls: ['./emotion.component.sass']
})
export class EmotionComponent implements OnInit {

  @Input() emotion;
  @Input() gridWidth;
  @Input() gridHeight;
  private selected : boolean = false;
  private orgWidth = 400;
  private orgHeight = 400;

  constructor(private emotionTracking : EmotionTrackingComponent) { }

  ngOnInit() {
  }

  toggle() {
    this.selected = !this.selected;
    this.emotionTracking.toggle(this.emotion.name);
  }

  private convertPosX(x) {
    return x * (this.gridWidth / this.orgWidth);
  }

  private convertPosY(y) {
    return y * (this.gridHeight / this.orgHeight);
  }
}
