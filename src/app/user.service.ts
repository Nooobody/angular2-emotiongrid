import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';

import { API_URL } from './globals';

import * as CryptoJS from "crypto-js";

@Injectable()
export class UserService {

  private token : String = "";
  private username : String = "";

  public is_logged_in = false;

  private tokenSource = new Subject<String>();
  private usernameSource = new Subject<String>();

  public tokenObs$ = this.tokenSource.asObservable();
  public usernameObs$ = this.usernameSource.asObservable();

  constructor(private http : Http, private router : Router) { }

  public sign_up(data) {
    this.send_request("user/sign_up", data)
      .subscribe(response => {
        this.handle_data(response);
        if (this.is_logged_in) {
          this.router.navigate(['/emotions']);
        }
      });
  }

  public login(username, password) {
    if (username === "test" && password === "test") {
      this.handle_data({
        token: "asdfjaskldjfgldskjf",
        username: "test"
      });
      this.router.navigate(['/emotions']);
      return;
    }
    
    this.send_request("user/login", {username: username, password: this.encrypt_password(password)})
      .subscribe(response => {
        this.handle_data(response);
        if (this.is_logged_in) {
          this.router.navigate(['/emotions']);
        }
      });
  }

  public logout() {
    this.send_request("user/logout", {token: this.token, username: this.username})
      .subscribe(response => {
        console.log(response)
        if (response.message === "success") {
          this.handle_data({token: "", username: ""});
          this.router.navigate(['/']);
        }
      });
  }

  public get_username() {
    return this.username;
  }

  public get_token() {
    return this.token;
  }

  private encrypt_password(password) {
    return CryptoJS.MD5(password).toString();
  }

  private handle_data(data) {
    console.log(data);
    if (data.token !== undefined) {
      this.token = data.token;
    }

    if (data.username !== undefined) {
      this.username = data.username;
    }

    this.update_data();
  }

  private update_data() {
    this.tokenSource.next(this.token);
    this.usernameSource.next(this.username);

    this.is_logged_in = this.token.length > 0;
  }

  private send_request(url, data) {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(API_URL + url, data, options).map(res => res.json());
  }
}
