import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { RouterModule } from '@angular/router';
import { EmotionTrackingComponent } from './emotion-tracking/emotion-tracking.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { NavigationComponent } from './navigation/navigation.component';
import { EmotionComponent } from './emotion/emotion.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './user.service';
import { LoggedInGuardGuard } from './logged-in-guard.guard';
import { WelcomeComponent } from './welcome/welcome.component';

@NgModule({
  declarations: [
    AppComponent,
    EmotionTrackingComponent,
    StatisticsComponent,
    NavigationComponent,
    EmotionComponent,
    SignUpComponent,
    LoginComponent,
    WelcomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', component: WelcomeComponent },
      { path: 'emotions', component: EmotionTrackingComponent, canActivate: [LoggedInGuardGuard] },
      { path: 'statistics', component: StatisticsComponent, canActivate: [LoggedInGuardGuard] }
    ])
  ],
  providers: [UserService, LoggedInGuardGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
