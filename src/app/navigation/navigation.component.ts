import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.sass']
})
export class NavigationComponent implements OnInit {

  private login = false;
  private signup = false;
  private logged_in = false;

  constructor(private user : UserService) {
    this.user.tokenObs$.subscribe(
      token => {
        if (token.length > 0) {
          this.logged_in = true;
          this.login = false;
          this.signup = false;
        }
        else {
          this.logged_in = false;
        }
      }
    );
  }

  ngOnInit() {
  }

  private show_login() {
    this.login = !this.login;
    this.signup = false;
  }

  private show_signup() {
    this.signup = !this.signup;
    this.login = false;
  }

  private logout() {
    this.user.logout();
  }
}
