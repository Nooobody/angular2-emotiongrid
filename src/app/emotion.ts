export interface Emotion {
  name : string;
  pos_x : number;
  pos_y : number;
  status : boolean;
}
