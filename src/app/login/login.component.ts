import { Component } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';

import { UserService } from '../user.service';
import { API_URL } from '../globals';
const url = API_URL + "user/login"

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent {

  private username;
  private password;

  constructor(private user : UserService) { }

  private login() {
    this.user.login(this.username, this.password);
  }
}
