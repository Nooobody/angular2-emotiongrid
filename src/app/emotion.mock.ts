import { Emotion } from './emotion';

export const EmotionMock : Emotion[] = [
  {name: "excited", pos_x: 42, pos_y: 124, status: false},
  {name: "happy", pos_x: 73, pos_y: 308, status: false},
  {name: "pleased", pos_x: 270, pos_y: 42, status: false},
  {name: "relaxed", pos_x: 108, pos_y: 60, status: false},
  {name: "peaceful", pos_x: 142, pos_y: 178, status: false},
  {name: "calm", pos_x: 274, pos_y: 217, status: false},
  {name: "sleepy", pos_x: 37, pos_y: 210, status: false},
  {name: "bored", pos_x: 300, pos_y: 139, status: false},
  {name: "sad", pos_x: 198, pos_y: 108, status: false},
  {name: "nervous", pos_x: 240, pos_y: 300, status: false},
  {name: "angry", pos_x: 138, pos_y: 242, status: false},
  {name: "anxious", pos_x: 54, pos_y: 20, status: false}
];
