import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmotionTrackingComponent } from './emotion-tracking.component';

describe('EmotionTrackingComponent', () => {
  let component: EmotionTrackingComponent;
  let fixture: ComponentFixture<EmotionTrackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmotionTrackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmotionTrackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have grid width and height', () => {
    expect(component.gridWidth).toBeTruthy();
    expect(component.gridHeight).toBeTruthy();
  });

  it('should toggle an emotion', () => {
    component.toggle("anxious");

    let emotion = component.emotions.find((x) => x.name == "anxious");
    expect(emotion.status).toBeTruthy();    
  });
});
