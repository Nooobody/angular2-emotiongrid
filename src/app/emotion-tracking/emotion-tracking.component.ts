import { Component, OnInit, ElementRef } from '@angular/core';
import { Emotion } from '../emotion';
import { EmotionMock } from '../emotion.mock';
import { Http, Headers, RequestOptions } from '@angular/http';
import { API_URL } from '../globals';
import 'rxjs/add/operator/map';
import { UserService } from '../user.service';

const url = API_URL + "emotion_tracking/"

@Component({
  selector: 'app-emotion-tracking',
  templateUrl: './emotion-tracking.component.html',
  styleUrls: ['./emotion-tracking.component.sass']
})
export class EmotionTrackingComponent implements OnInit {

  public emotions : Emotion[] = [];
  public gridWidth : number;
  public gridHeight : number;

  private username : String = "";

  constructor(private http : Http, private el : ElementRef, private user : UserService) {
    this.user.usernameObs$.subscribe(
      username => {
        this.username = username;
      }
    )
  }

  ngOnInit() {
    this.emotions = EmotionMock;
    this.updateSize();
  }

  public toggle(name) {
    let emotion : Emotion = this.emotions.find((x) => x.name === name);
    emotion.status = !emotion.status;
  }

  private updateSize() {
    let grid = this.el.nativeElement.firstChild.children[1];

    this.gridWidth = grid.clientWidth;
    this.gridHeight = grid.clientHeight;
  }

  private confirm() {
    let emotions = this.emotions.filter((x) => x.status).map((x) => x.name);

    this.sendEmotions(emotions).subscribe((response) => console.log(response));
  }

  private sendEmotions(emotions) {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, {"data": emotions, "token": this.user.get_token()}, options)
           .map(response => response.json());
  }
}
